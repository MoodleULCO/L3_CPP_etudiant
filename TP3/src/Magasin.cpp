#include <iostream>
#include <string>
#include <vector>

#include "Produit.hpp"
#include "Client.hpp"
#include "Location.hpp"
#include "Magasin.hpp"

Magasin::Magasin(){
	_idCourantClient = 0;
	_idCourantProduit = 0;
}

/*-------------------------Client----------------------------*/
int Magasin::nbClients() const{
	return _client.size();
}

void Magasin::ajouterClient(const std::string & nom){
	_client.push_back(Client(_idCourantClient, nom));
	_idCourantClient ++;
}

void Magasin::afficherClients() const{
	std::cout << " Clients : " << std::endl;
	for(int i=0; i< _client.size(); i++){
		std::cout << " -> ";
		_client.at(i).afficherClient();
	}
}

void Magasin::supprimerClient(int idClient){
	try{
		for(int i = 0; i < _client.size(); i++){
			if(_client.at(i).getId() == idClient){
				std::swap(_client.at(i), _client.back());
				_client.pop_back();
				std::cout << "\nClient " << i << " supprimé" << std::endl;
			}
		}
	}catch(const std::exception & e){
		std::cerr << "Erreur, client " << idClient << " inexistant" << std::endl;
	}
}


/*-------------------------Produit----------------------------*/
int Magasin::nbProduits() const{
	return _produit.size();
}

void Magasin::ajouterProduit(const std::string & nom){
	_produit.push_back(Produit(_idCourantProduit, nom));
	_idCourantProduit ++;
}

void Magasin::afficherProduits() const{
	std::cout << " Produits : " << std::endl;
	for(int i=0; i< _produit.size(); i++){
		std::cout << " -> ";
		_produit.at(i).afficherProduit();
	}
}

void Magasin::supprimerProduit(int idProduit){
	try{
		for(int i = 0; i < _produit.size(); i++){
			if(_produit.at(i).getId() == idProduit){
				std::swap(_produit.at(i), _produit.back());
				_produit.pop_back();
				std::cout << "\nProduit " << i << " supprimé" << std::endl;
			}
		}
	}catch(const std::exception & e){
		std::cout << "Erreur, produit " << idProduit << " inexistant" << std::endl;
	}
}
