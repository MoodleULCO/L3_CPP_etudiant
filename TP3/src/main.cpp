#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"

int main(){
	Location loc;
	loc._idClient = 1;
	loc._idProduit = 6;
	
	loc.afficherLocation();

	Client cli(1, "Rachid");
	cli.afficherClient();

	Produit pdt(1, "Carchacrokite");
	pdt.afficherProduit();

	Magasin mag;
	mag.ajouterClient("Rachid");
	mag.ajouterClient("Mehmoud");
	mag.ajouterClient("Gigi");
	std::cout << "\nNb clients : " << mag.nbClients() << std::endl;
	mag.afficherClients();
	
	mag.supprimerClient(1);
	mag.afficherClients();

	mag.ajouterProduit("Lubrifiant");
	mag.ajouterProduit("Trappe à skis");
	std::cout << "\nNb Produits : " << mag.nbProduits() << std::endl;	
	mag.afficherProduits();

	mag.supprimerProduit(0);
	mag.afficherProduits();

	mag.supprimerClient(12);
}


