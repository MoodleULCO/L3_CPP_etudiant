#include "Client.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GpClient) { };

TEST(GpClient, test_1) {
    Client cli (1, "Rachid");
    CHECK_EQUAL(cli.getId(), 1);
    CHECK_EQUAL(cli.getNom(), "Rachid");
}
