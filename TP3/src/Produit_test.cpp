#include "Produit.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GpProduit) { };

TEST(GpProduit, test_1) {
    Produit pdt (1, "Carchacrokite");
    CHECK_EQUAL(pdt.getId(), 1);
    CHECK_EQUAL(pdt.getDescription(), "Carchacrokite");
}
