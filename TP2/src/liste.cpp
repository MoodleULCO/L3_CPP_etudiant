#include "liste.h"

Liste::Liste()
{
    _tete = nullptr;
}


void Liste::ajouterDevant(int valeur){
    Noeud *nouveau = new Noeud;
    nouveau -> _valeur = valeur;
    nouveau -> _suivant = _tete;
    _tete = nouveau;
}

int Liste::getTaille(){
    int taille = 0;

    Noeud *nouveau = _tete;

    while(nouveau->_suivant != nullptr){
        taille++;
        nouveau = nouveau->_suivant;
    }

    return taille;
}

int Liste::getElement(int indice){
    int value;
    int i= 0;

    Noeud *nouveau = _tete;

    while(i < indice){
        nouveau = nouveau->_suivant;
        i++;
    }
    value = nouveau->_valeur;

    return value;
}

Liste::~Liste(){
    Noeud *n =_tete;
    while(n != nullptr){
        Noeud *nn = n->_suivant;
        delete n;
        n = nn;
    }
}
