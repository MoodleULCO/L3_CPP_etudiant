#include "Doubler.hpp"

#include <iostream>
#include "liste.h"

int main() {
    /*int a = 42;
    int *p = &a;
    *p = 37;

    int *t = new int [10];
    t[3] = 42;
    delete [] t;
    t = nullptr;*/

    Liste *liste = new Liste();
    liste->ajouterDevant(3);
    liste->ajouterDevant(54);

    if(liste->getTaille()!=0){
        for(int i=0; i < liste->getTaille(); i++){
            int x = liste->getElement(i);
            std::cout << x << " -> ";
        }
        std::cout << liste->getElement(liste->getTaille()) << std::endl;
    }
    std::cout << liste->getTaille() << std::endl;

    return 0;
}

