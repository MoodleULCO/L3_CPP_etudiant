#ifndef LISTE_H
#define LISTE_H

struct Noeud
{
    int _valeur;
    Noeud *_suivant;
};

struct Liste
{
    Noeud *_tete;
    Liste();
    void ajouterDevant(int valeur);
    int getTaille();
    int getElement(int indice);
    ~Liste();
};

#endif // LISTE_H
