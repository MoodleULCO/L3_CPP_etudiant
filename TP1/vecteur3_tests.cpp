#include <CppUTest/CommandLineTestRunner.h>
#include "vecteur3.h"

TEST_GROUP(GpVecteur) { };
TEST(GpVecteur, test_1) {
    Vecteur3 vecteur {2,3,6};
    double result = vecteur.norme();
    CHECK_EQUAL(7, result);
}

TEST(GpVecteur, test_2) {
    Vecteur3 vecteur {2,3,6};
    Vecteur3 vecteur2 {1,2,3};
    double pS = ((2*1) + (3*2) + (6*3));
    double result = produitScalaire(vecteur, vecteur2);
    CHECK_EQUAL(pS, result);
}

TEST(GpVecteur, test_3) {
    Vecteur3 vecteur {2,3,6};
    Vecteur3 vecteur2 {1,2,3};
    Vecteur3 vecteur3 {3,5,9};
    Vecteur3 result = addition(vecteur, vecteur2);
    CHECK_EQUAL(vecteur3.x, result.x);
    CHECK_EQUAL(vecteur3.y, result.y);
    CHECK_EQUAL(vecteur3.z, result.z);
}
