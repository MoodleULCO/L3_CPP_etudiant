#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GpFibonacci) { };
TEST(GpFibonacci, test_1) {
    int result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);

    result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);

    result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);

    result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);

    result = fibonacciRecursif(5);
    CHECK_EQUAL(5, result);
}

TEST(GpFibonacci, test_2) {
    int listResult[] = {1,1,2,3,5};

    for(int i = 1; i <=5; i++){
        CHECK_EQUAL(listResult[i-1], fibonacciIteratif(i));
    }
}
