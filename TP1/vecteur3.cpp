#include <iostream>
#include "vecteur3.h"
#include <math.h>


void afficher(Vecteur3 v){
    std::cout  << "Vecteur : (" << v.x << ", " << v.y << ", " << v.z << ")" << std::endl;
}

void Vecteur3::afficher(){
    std::cout << "Vecteur : (" << x << ", " << y << ", " << z << ")" << std::endl;
}
double Vecteur3::norme(){
    double resultat = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
    return resultat;
}

float produitScalaire(Vecteur3 v1, Vecteur3 v2){
    float resultat = ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z));
    return resultat;
}

Vecteur3 addition(Vecteur3 v1, Vecteur3 v2){
    Vecteur3 v3;
    v3.x = (v1.x + v2.x);
    v3.y = (v1.y + v2.y);
    v3.z = (v1.z + v2.z);
    return v3;
}
