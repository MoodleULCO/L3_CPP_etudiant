#include <iostream>
#include "fibonacci.hpp"
#include "vecteur3.h"

int main ()
{ 
	std::cout << "Itératif : " << fibonacciIteratif(7) << std::endl;
	std::cout << "Récursif : " << fibonacciRecursif(7) << std::endl;

    Vecteur3 vecteur{2,3,6};
    Vecteur3 vecteur2{1,2,3};

    vecteur.afficher();
    afficher(vecteur2);

    std::cout << "Norme : " << vecteur.norme() << std::endl;
    std::cout << "Produit Scalaire : " << produitScalaire(vecteur, vecteur2) << std::endl;

    Vecteur3 v3 = addition(vecteur, vecteur2);
    afficher(v3);

}
