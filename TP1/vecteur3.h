#ifndef VECTEUR3_H
#define VECTEUR3_H

/*class Vecteur3 {
    private:
        float x, y, z;
    public:
        Vecteur3();
        Vecteur3(float a, float b, float c);
        void afficher();
};*/

struct Vecteur3
{
    float x, y, z;
    void afficher();
    double norme();
};

void afficher(Vecteur3 v);
float produitScalaire(Vecteur3 v1, Vecteur3 v2);
Vecteur3 addition(Vecteur3 v1, Vecteur3 v2);

#endif // VECTEUR3_H
