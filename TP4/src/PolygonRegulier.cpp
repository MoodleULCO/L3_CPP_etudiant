#include <iostream>
#include <string>
#include <math.h>

#include "Couleur.hpp"
#include "FigureGeometrique.hpp" 
#include "Point.hpp"	
#include "PolygonRegulier.hpp"

PolygonRegulier::PolygonRegulier(const Couleur & couleur, const Point & centre, int Rayon, int nbCotes) : FigureGeometrique(couleur)
{
	_nbPoints = nbCotes;
	_points = new Point[nbCotes];
	
	for (int i = 0; i < nbCotes; i ++) {
		_points[i]._x = Rayon*cos(i*(M_PI/nbCotes))+centre._x;
		_points[i]._y = Rayon*sin(i*(M_PI/nbCotes))+centre._y;
	}
}

void PolygonRegulier::afficher() const {
	std::cout << "PolygoneRegulier " << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b 
	<< " ";

	for(int i = 0; i < _nbPoints; i++){
		std::cout << _points[i]._x << "_" << _points[i]._y << " ";
	}
	std::cout << std::endl;
}

int PolygonRegulier::getNbPoints() const {
	return _nbPoints;
}

const Point & PolygonRegulier::getPoint(int indice) const {
	for(int i = 0; i < _nbPoints; i++){
		if(i == indice){
			return _points[i];
		}
	}
	std::cout << "Pas de point a cet indice" << std::endl;
}

PolygonRegulier::~PolygonRegulier(){
	delete[] _points;
}

