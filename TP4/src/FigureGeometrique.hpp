#ifndef _FIGURE_GEOMETRIQUE_
#define _FIGURE_GEOMETRIQUE_

#include <iostream>
#include <string>
#include "Couleur.hpp"

class FigureGeometrique{
	protected:
		Couleur _couleur;
	public:
		FigureGeometrique(const Couleur & couleur);
		const Couleur & getCouleur() const;
};

#endif
