#ifndef _POLYGON_REGULIER_
#define _POLYGON_REGULIER_

#include "Couleur.hpp"
#include "FigureGeometrique.hpp" 
#include "Point.hpp"

class PolygonRegulier : public FigureGeometrique {
	private:
		int _nbPoints;
		Point *_points;
	public:
		PolygonRegulier(const Couleur & couleur, const Point & centre, int Rayon, int nbCotes);
		void afficher() const;
		int getNbPoints() const;
		const Point & getPoint(int indice) const;
		~PolygonRegulier();
	
};

#endif
