#include <iostream>
#include <string>

#include "Couleur.hpp"
#include "FigureGeometrique.hpp" 
#include "PolygonRegulier.hpp" 
#include "Point.hpp"
#include "Ligne.hpp"

int main(){
	
	Point point0;
	point0._x = 0;
	point0._y = 0;
	
	Point point1;
	point1._x = 100;
	point1._y = 200;

	Couleur couleur;
	couleur._r = 1;
	couleur._g = 0;
	couleur._b = 0;

	Ligne ligne(couleur, point0, point1);
	ligne.afficher();
	
	PolygonRegulier poly(couleur, point1, 50, 4);
	poly.afficher();
	std::cout << poly.getPoint(2)._x << "_" << poly.getPoint(2)._y << std::endl;

	return 0;
}
