#ifndef _LIGNE_
#define _LIGNE_

#include <iostream>
#include <string>

#include "Couleur.hpp"
#include "FigureGeometrique.hpp" 
#include "Point.hpp"

class Ligne : public FigureGeometrique {
	private:
		Point _p0;
		Point _p1;
	public:
		Ligne(const Couleur & couleur, const Point & p0, const Point & p1);
		const Point & getP0() const;
		const Point & getP1() const; 
		void afficher() const;
};

#endif
