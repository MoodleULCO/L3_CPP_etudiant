#ifndef IMAGE_CPP
#define IMAGE_CPP
#include "Image.hpp"
#include <fstream>
#include <string>
Image::Image(int largeur, int hauteur){
	_largeur = largeur;
	_hauteur = hauteur;
	_pixels = new int(_largeur*_hauteur);
}
int Image::getHauteur() const{
	return _hauteur;
}
int Image::getLargeur() const{
	return _largeur;
}
/*int Image::getPixel(int i, int j) const{
	return _pixels[i+(j*_largeur)];
}
void Image::setPixel(int i, int j, int couleur){
	if(couleur > 255){
		couleur = 255;
		_pixels[i+(j*_largeur)] = couleur;
	}
	else if(couleur < 0){
			couleur = 0;
			_pixels[i+(j*_largeur)] = couleur;
	}
	else{
		_pixels[i+(j*_largeur)] = couleur;
	}
}*/
const int& Image::pixel(int i, int j) const{
	if(i <= _largeur && j <= _hauteur && i >= 0 && j >= 0){
		return _pixels[i+(j*_largeur)];
	}
	return _pixels[i+(j*_largeur)];	
}
int & Image::pixel(int i, int j){
	if(i <= _largeur && j <= _hauteur && i >= 0 && j >= 0){
		return _pixels[i+(j*_largeur)];
	}
	return _pixels[i+(j*_largeur)];
}
void Image::ecrirePnm(const Image & img, const std::string & nomFichier){
	std::ofstream ofs(nomFichier,std::ofstream::out);
	ofs << "P2 "+std::to_string(img.getLargeur())+" "+std::to_string(img.getHauteur())+" 255\n";
	for(int i=0; i <= img.getHauteur(); i++){
		for(int j=0; j <= img.getLargeur(); j++){
                        ofs << std::to_string(img.pixel(j,i))+" ";
		}
		ofs << "\n";
	}
	ofs.close();
}
Image::~Image(){
	delete [] _pixels;
}
#endif
