#ifndef BIBLIO_HPP_
#define BIBLIO_HPP_

#include <iostream>
#include <string>

class Bibliotheque : public Livre{
	public :
		void Afficher() const;
		void trierParAuteurEtTitre();
		void trierParAnnee();
		void lireFichier(const std::string & nomFichier);
		void ecrireFichier()const std::string & nomFichier) const;
};

#endif
